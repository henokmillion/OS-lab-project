/*-------------------------------
 * functions of TA's activities
 *-------------------------------
*/

/*
 * help student
 * @arg: t (void *): duration of session
 * @return: void *
 *
*/
void *help(void *t)
{
	// elapse some time
	sleep(rand() % 4);
	// check for other students
}

/*
 * sleep (literally)
 * @arg: t: duration of sleep
 * @return: void *
 *
*/
void *_sleep(void *t)
{
	// sleep for time t
	// wake up if awoken
	printf("sleeeping\n");
	sem_wait(&sleep_mutex);
	sleep(rand() % 10);
}
