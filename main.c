/*
 * main.c
 *
 * OS lab project
 * ---------------
 *  Eyoel Wondwossen
 *  Henok Million
 *  Robel Ephraim
 *  Zebiba Hassen
 *  
*/

#include "headers.h"



void main()
{
	// create mutexes
	// states: sleeping, programming, assisting, waiting
	// create threads for students and a TA
	pthread_t student[10];
	pthread_t ta;
	// create semaphores (maybe scrap mutexes)
	if(sem_init(&assist_mutex, 0, 1) < 0)
	{
		printf("error initializing assist_mutex");
	}
	if(sem_init(&wait_sem, 0, 3) < 0)
	{
		printf("error initializing wait_sem");
	}
	if(sem_init(&sleep_mutex, 0, 1) < 0)
	{
		printf("error initializing sleep_mutex");
	}

	for (int i = 0; i < 10; i++)
	{
		if(pthread_create(&student[i], NULL, &program, NULL) != 0)
		{
			printf("error creating student thread %d", i);
		}
		printf("%d\n", i);
	}
	if(pthread_create(&ta, NULL, &_sleep, NULL) != 0)
	{
		printf("error creating ta thread");
	}
	if(pthread_join(ta, NULL) != 0)
	{
		printf("error waiting for ta thread to finish");
	}
	for (int i = 0; i < 10; i++)
	{
		if (pthread_join(student[i], NULL) != 0)
		{
			printf("error waiting for student thread %d to finish", i);
		}
	}
	if(sem_destroy(&assist_mutex) < 0)
	{
		printf("error destroying assit_mutex semaphore");
	}
	if(sem_destroy(&wait_sem) < 0)
	{
		printf("error destroying wait_sem semaphore");
	}
	if(sem_destroy(&sleep_mutex) < 0)
	{
		printf("error distroying sleep_mutex semaphore");
	}
	// make things work
}
