/*------------------------------------
 * functions of student's activities
 *------------------------------------
*/

void *program(void *t);
void wakeTA(void *t);

/*
 * seek assistance from ta
 *
 * @return: void
*/
void seekAssistance()
{
	int sleep_val, queue_val;
	if (sem_getvalue(&wait_sem, &queue_val) < 0)
	{
		printf("error getting wait_sem value");
	};
	printf("queue_val: %d\n", queue_val);
	if (queue_val < 1)
	{
		program(NULL);
	}
	// wake teacher up if someone is at the door

	if(sem_getvalue(&sleep_mutex, &sleep_val) < 0)
	{
		printf("error getting sleep_mutex value\n");
	}
	if(sem_getvalue(&sleep_mutex, &sleep_val) < 0)
	{
		printf("error getting sleep_mutex value\n");
	}
	printf("thread id: %ld\n", pthread_self());	
	if (sleep_val < 1)
	{
		wakeTA(NULL);
	}
	if (sem_wait(&sleep_mutex) < 0)
	{
		printf("error waiting for sleep_mutex");
	}
	if (sem_wait(&wait_sem) < 0)
	{
		printf("error waiting for wait_sem");
	}
	if (sem_wait(&assist_mutex) < 0)
	{
		printf("error waiting for assist_mutex");
	}
	// do the assistancie
	printf("assisting...\n");
	help(NULL);

	if (sem_post(&wait_sem) < 0)
	{
		printf("error incrementing wait_sem");
	}
	if (sem_getvalue(&wait_sem, &queue_val) < 0)
	{
		printf("error getting value of wait_sem");
	}
	if(sem_post(&assist_mutex) < 0)
	{
		printf("error incrementing assist_mutex");
	}
	// check if anyone is waiting outside
	// go to sleep if no one is
	if (queue_val == 3)
	{
		printf("no more threads on queue, so sleeping...\n");
		if(sem_post(&sleep_mutex) < 0)
		{
			printf("error incrementing sleep_mutex");
		}
		_sleep(NULL);
	}
}

/* 
 * do programming
 *
 * @arg t: void *
 * @return: void *
 *
*/
void *program(void *t)
{
	while(1)
	{
		// elapse some time programming
		printf("programming...\n");
		int randtime = rand();
		if (randtime > 10)
		{
			randtime = randtime % 20;
			
		}
		sleep(randtime);
		seekAssistance(NULL);
	}
}

/*
 * wake up TA
 *
 * @arg: p
 * @return: void *
 *
 *
*/
void wakeTA(void *p)
{
	// wake TA
	// ask for help
	sem_post(&sleep_mutex);
}
